#' @importFrom rlang .data
#' @importFrom grDevices pdf dev.off

# SPR ODE functions
spr_association <- function(t, y, params) {
  with(as.list(c(y, params)), {
    L_0 <- params["L_0"]
    
    ka_g1 <- params["ka_g1"]
    ka_b1 <- params["ka_b1"]
    ka_g2 <- params["ka_g2"]
    ka_b2 <- params["ka_b2"]
    kd_g1 <- params["kd_g1"]
    kd_b1 <- params["kd_b1"]
    kd_g2 <- params["kd_g2"]
    kd_b2 <- params["kd_b2"]
    
    N_g <- params["N_g"]
    N_b <- params["N_b"]
    
    dAdt <- ka_g1 * N_g * L_0 + kd_b2 * C - kd_g1 * A
    dBdt <- ka_b1 * N_b * L_0 + kd_g2 * C - kd_b1 * B
    dCdt <- ka_b2 * N_b * A + ka_g2 * N_g * B - kd_g2 * C - kd_b2 * C
  
    return(list(c(dAdt, dBdt, dCdt)))
  })
}


spr_dissociation <- function(t, y, params) {
  with(as.list(c(y, params)), {

    kd_g1 <- params["kd_g1"]
    kd_b1 <- params["kd_b1"]
    kd_g2 <- params["kd_g2"]
    kd_b2 <- params["kd_b2"]
    
    dAdt <- kd_b2 * C - kd_g1 * A
    dBdt <- kd_g2 * C - kd_b1 * B
    dCdt <- -kd_g2 * C - kd_b2 * C
    
    return(list(c(dAdt, dBdt, dCdt)))
  })
}

# This function is used internally. It is a replacement for summary.nlm that allows for a non-singular
# Hessian due to the constrained fits.

# test: fit_object <- fits_list[[well_idx]]$result$FitResult
summary_fit_with_constraints_para <- function(fit_object){
  
  info <- fit_object$info
  hessian <- fit_object$hessian
  pars <- fit_object$par
  n <- length(pars)
  std_err_full <- rep(NA, n)
  
  if (info == 0 | info == 5) {
    df <- data.frame(Estimate = pars, "Std. Error" = std_err_full)
    colnames(df) <- c("Estimate", "Std. Error")
    return(df)
  }
  
  n <- nrow(hessian)
  test_zeroes <- apply(hessian, 1 , function(x) sum(x==0))
  nonsingular_rows <- which(test_zeroes != n)
  # if (length(nonsingular_rows) == n & info != 5)
  #    return(summary(fit_object)$coefficients)
  hessian <- hessian[nonsingular_rows, nonsingular_rows]
  
  std_err_full <- rep(NA, n)
  
  # get table directly. Code is pulled from summary.minpack.lm
  
  if (info != 5) {            # when info is 5, that means the iterations maxed out and fit is not valid
    ibb <- chol(hessian)
    ih <- chol2inv(ibb)
    p <- length(pars)
    rdf <- length(fit_object$fvec) - p
    resvar <- stats::deviance(fit_object)/rdf
    se <- sqrt(diag(ih) * resvar)
  }
  else
    se <- rep(NA, length(nonsingular_rows))
  
  std_err_full[nonsingular_rows] <- se
  
  df <- data.frame(Estimate = pars, "Std. Error" = std_err_full)
  colnames(df) <- c("Estimate", "Std. Error")
  df
}


# Internal function that is passed to nlm. It computes the objective function for the fit.
# test: pars_fit <- init_params
# pars_fit <- pars
# problem: bulkshift?
fit_as_system_para <- function(pars_fit, 
                               pars_known,
                               df, 
                               incl_concentrations,
                               num_conc,
                               association,
                               bulkshift,
                               global_rmax){
  # print(pars_fit)
  # pars_fit <- c(9.915547e+01, 6.855083e+01, 4.177412e+01, 2.044079e+01,
  #               8.321175e+00 , 1.031721e+04, 1.031721e+04, 1.441712e-04,
  #               1.089876e-05)
  
  # deleted: regenerated_surface
  
  names(pars_fit) <- rep("", length(pars_fit
                                    ))
  if(global_rmax){
    
    L_0 <-  pars_fit[1]
    
    parameter_values <- c(ka_g2 =  pars_fit[2], ka_b2 =  pars_fit[3],
                          kd_g2 =  pars_fit[4], kd_b2 =  pars_fit[5])
    
    if (bulkshift)
     shift <-  pars_fit[6:(5 + num_conc)]
    
  } else{
    L_0 <-  pars_fit[1:num_conc]
    
    parameter_values <- c(ka_g2 =  pars_fit[num_conc + 1],
                          ka_b2 =  pars_fit[num_conc + 2],
                          kd_g2 =  pars_fit[num_conc + 3],
                          kd_b2 =  pars_fit[num_conc + 4])
    
    if (bulkshift)
     shift <-  pars_fit[(num_conc + 5):(2*num_conc + 4)]
}

  # error   
  err_assoc <- NULL
  err_dissoc <- NULL
  
  # test: i<-1
  for (i in 1:num_conc){
    
    df_i <- df %>% dplyr::filter(.data$Concentration == incl_concentrations[i])
    
    df_i %>% dplyr::filter(.data$AssocIndicator == 1) -> df_assoc
    df_i %>% dplyr::filter(.data$DissocIndicator == 1) -> df_dissoc
    
    # set all parameter values
    if (global_rmax){    
      parameter_values_all <- c(L_0 = L_0, 
                                pars_known,
                                parameter_values,
                                N_g = incl_concentrations[i]/2,
                                N_b = incl_concentrations[i]/2)
      
    } else {
      parameter_values_all <- c(L_0 = L_0[i], 
                                pars_known,
                                parameter_values,
                                N_g = incl_concentrations[i]/2,
                                N_b = incl_concentrations[i]/2)
    }
    
    # solve ODE
    # association
    y0 <- c(A = 0, B = 0, C = 0)
    sol_assoc <- as.data.frame(deSolve::ode(y = y0, 
                                            times = df_assoc$Time, 
                                            func = spr_association, 
                                            parms = parameter_values_all))
    
    colnames(sol_assoc) <- c("Time", "A", "B", "C")
    
    assoc_value <- sol_assoc$A + sol_assoc$B + 2*sol_assoc$C
    err_assoc <- c(err_assoc, df_assoc$RU - assoc_value)  
    
    # End of association
    y_asso_end <- unlist(tail(sol_assoc, 1))[-1]
    
    # Dissociation
    parameter_values_dissoc <- parameter_values_all[c("kd_g1", "kd_b1", "kd_g2", "kd_b2")]
    
    # Problem: method choose
    sol_dissoc <- as.data.frame(deSolve::ode(y = y_asso_end, 
                                             times = df_dissoc$Time, 
                                             # problem: - association,
                                             func = spr_dissociation, 
                                             parms = parameter_values_dissoc))
    
    colnames(sol_dissoc) <- c("Time", "A", "B", "C")
    dissoc_value <- sol_dissoc$A + sol_dissoc$B + 2*sol_dissoc$C
    err_dissoc <- c(err_dissoc, df_dissoc$RU - dissoc_value)
    
    # df11 <- cbind.data.frame(df_assoc, assoc_value, err_assoc)
    # View(df11)
    #df2 <- cbind.data.frame(df_dissoc, dissoc_value, err_dissoc)
    # View(df2)
  }
  
  c(err_assoc, err_dissoc)
}

# # plot
# spr_values <- rbind.data.frame(sol_assoc, sol_dissoc)
# 
# # Plot
# spr_values %>%
#   as_tibble() %>%
#   pivot_longer(cols = c(A, B, C), names_to = "Species", values_to = "Concentration") %>%
#   ggplot(aes(x = Time, y = Concentration, col = Species)) +
#   geom_line() 
# 
# Internal. Called by fit_association_dissociation

get_fit_outcomes_para <- function(L_0, 
                             parameter_values,
                             df, num_conc,
                             incl_concentrations,
                             association,
                             shift,
                             global_rmax){
  
  full_output_RU <- NULL
  
  for (i in 1:num_conc){
    
    df_i <- df %>% dplyr::filter(Concentration == incl_concentrations[i])
    Time <- df_i$Time
    Concentration <- df_i$Concentration
    
    df_i %>% dplyr::filter(.data$AssocIndicator == 1) -> df_assoc
    df_i %>% dplyr::filter(.data$DissocIndicator == 1) -> df_dissoc
    
    if (global_rmax){    
      parameter_values_all <- c(L_0 = L_0, 
                                parameter_values,
                                N_g = incl_concentrations[i]/2,
                                N_b = incl_concentrations[i]/2)   
      
    } else {
      parameter_values_all <- c(L_0 = L_0[i], 
                                parameter_values,
                                N_g = incl_concentrations[i]/2,
                                N_b = incl_concentrations[i]/2)  
    }
    
    # Problem: relationship between get_fit_outcome and fit_as_system
    # is y still 000?
    y0 <-c(A = 0, B = 0, C = 0)
    
    sol_assoc <- as.data.frame(deSolve::ode(y = y0, 
                                            times = df_assoc$Time, 
                                            func = spr_association, 
                                            parms = parameter_values_all))
    
    colnames(sol_assoc) <- c("Time", "A", "B", "C")
    
    df_assoc$RU <- sol_assoc$A + sol_assoc$B + 2*sol_assoc$C
    
    # End of association
    # shift is zero if no bulkshift
    if (shift) {
      y_asso_end <- unlist(tail(sol_assoc, 1))[-1] + shift[i]
    } else {
      y_asso_end <- unlist(tail(sol_assoc, 1))[-1]
    }
    
    # Dissociation
    sol_dissoc <- as.data.frame(deSolve::ode(y = y_asso_end, 
                                             times = df_dissoc$Time - association, 
                                             func = spr_dissociation, 
                                             parms = parameter_values_all))
    
    colnames(sol_dissoc) <- c("Time", "A", "B", "C")
    
    df_dissoc$RU <- sol_dissoc$A + sol_dissoc$B + 2*sol_dissoc$C
    
    full_output_RU <- dplyr::bind_rows(full_output_RU, df_assoc, df_dissoc)
  }
  # return fitted values
  full_output_RU %>% dplyr::select("Time", "RU", "Concentration")
}

# internal function. Fits sensorgrams for a given well
# test:
# well_idx <- 1
# sample_info <- sample_info_fits
# x_vals <- Time[, keep_concentrations]
# y_vals <- RU[, keep_concentrations]
# min_allowed_kd <- 10^(-5)
# max_iterations <- 500
# max_iterations <- 500
# ptol <- 10^(-10)
# ftol <- 10^(-10)

fit_association_dissociation_para <- function(well_idx, sample_info, 
                                         pars_individual,
                                         x_vals, y_vals,
                                         incl_concentrations_values, n_time_points,
                                         min_allowed_kd = 10^(-5),
                                         max_iterations = 500,
                                         ptol = 10^(-10),
                                         ftol = 10^(-10),
                                         max_RU_tol){
  
  # this function will fit all selected concentrations for one well
  baseline <- sample_info[well_idx,]$Baseline
  baseline_start <- sample_info[well_idx,]$`Bsl Start`
  
  pars_known <- c(ka_g1 = pars_individual[well_idx, 1], kd_g1 = pars_individual[well_idx, 2],
                  ka_b1 = pars_individual[well_idx, 3], kd_b1 = pars_individual[well_idx, 4])
  
  if (sample_info[well_idx,]$Bulkshift == "Y")
    bulkshift <- TRUE else
      bulkshift <- FALSE
  
  if (sample_info[well_idx,]$`Global Rmax` == "Y")
    global_rmax <- TRUE else
      global_rmax <- FALSE
  
  if (sample_info[well_idx,]$`Regen.` == "Y" |
      sample_info[well_idx,]$BaselineNegative)
    regenerated_surface <- TRUE else
      regenerated_surface <- FALSE
  
  start_idx <- sample_info[well_idx,]$FirstInclConcIdx
  num_conc <- sample_info[well_idx,]$NumInclConc
  end_idx <- start_idx + num_conc - 1
  
  association <- sample_info[well_idx,]$Association
  dissociation <- sample_info[well_idx,]$Dissociation
  
  assoc_start <- baseline + baseline_start
  assoc_end <- assoc_start + association
  
  dissoc_start <- assoc_end
  dissoc_end <- assoc_end + dissociation
  
  if (sample_info[well_idx,]$`Automate Dissoc. Window` == "Y" &
      (is.finite(sample_info[well_idx, ]$DissocEnd)))
    dissoc_end <- sample_info[well_idx,]$DissocEnd
  
  n_vals <- dim(x_vals)[2]
  names(x_vals) <- as.character(1:n_vals)
  names(y_vals) <- as.character(1:n_vals)
  
  Time <- x_vals[, start_idx:end_idx] %>%
    tidyr::pivot_longer(cols = tidyselect::everything()) %>%
    dplyr::arrange(as.numeric(.data$name)) %>%
    dplyr::select("value")
  RU <- y_vals[, start_idx:end_idx]%>%
    tidyr::pivot_longer(cols = tidyselect::everything()) %>%
    dplyr::arrange(as.numeric(.data$name)) %>%
    dplyr::select("value")
  
  incl_concentrations <-
    incl_concentrations_values[start_idx:end_idx]
  
  purrr::map_dfr(.x = tibble::tibble(incl_concentrations),
                 .f = function(x, n_time_points) rep(x, n_time_points), n_time_points) %>%
    dplyr::arrange(.data$incl_concentrations) -> incl_concentrations_rep
  
  df <- suppressMessages(dplyr::bind_cols("Time" = Time,
                                          "RU" = RU,
                                          "Concentration" = incl_concentrations_rep))
  colnames(df) <- c("Time", "RU", "Concentration") #force correct names - dplyr is doing weird things
  
  
  #do both dissociation and association
  df %>% dplyr::mutate(AssocIndicator =
                         ifelse((.data$Time >= assoc_start & .data$Time < assoc_end), 1, 0),
                       DissocIndicator = ifelse(Time > dissoc_start & Time < dissoc_end, 1, 0)) -> df
  df %>% dplyr::filter((.data$AssocIndicator == 1 | .data$DissocIndicator == 1)) -> df
  
  df %>% dplyr::group_by(.data$Concentration) %>% dplyr::summarise(max = max(.data$RU, na.rm = TRUE)) -> Rmax_start_df
  df %>% dplyr::group_by(.data$Concentration) %>% dplyr::summarise(min = min(.data$RU, na.rm = TRUE)) -> R0_start
  
  
  # Consider initial ligand concentration as the maximum RU for a given time series
  if (global_rmax){
    L_0_start <- max(Rmax_start_df$max, na.rm = TRUE)
  } else {
    L_0_start <- Rmax_start_df$max
  }
  
  # ka_g2_start <- ka_b2_start <- 10^(5)
  # kd_g2_start <- kd_b2_start <- 10^(-5)
  
  ka_g2_start <- pars_known["ka_g1"]
  ka_b2_start <- pars_known["ka_b1"]
  kd_g2_start <- pars_known["kd_g1"]
  kd_b2_start <- pars_known["kd_b1"]
  
  # shift time to start at zero
  df$Time <- df$Time - assoc_start
  
  # bulkshift initial value
  shift <- rep(0, num_conc)
  
  if (bulkshift & !regenerated_surface){
    init_params <- c(L_0_start,
                     ka_g2_start, ka_b2_start,
                     kd_g2_start, kd_b2_start,
                     shift)
    
    # Problem: set the numbers correctly?
    param_lower_bounds <- c(rep(0, length(L_0_start)),
                            rep(10, 2),
                            rep(min_allowed_kd, 2), 
                            rep(-100, num_conc))
    
    param_upper_bounds <- c(rep(max_RU_tol, length(L_0_start)),
                            rep(10^7,2), 
                            rep(1, 2),
                            rep(100, num_conc))
    
  } else { if (!bulkshift & !regenerated_surface){
    init_params <- c(L_0_start,
                      ka_g2_start, ka_b2_start,
                      kd_g2_start, kd_b2_start)
    param_lower_bounds <- c(rep(0, length(L_0_start)),
                            rep(10, 2), 
                            rep(min_allowed_kd,2))
    
    param_upper_bounds <- c(rep(max_RU_tol, length(L_0_start)),
                            rep(10^7,2),  
                            rep(1, 2))
    
  } else { if (bulkshift & regenerated_surface){
    init_params <- c(L_0_start,
                     ka_g2_start, ka_b2_start, 
                     kd_g2_start, kd_b2_start,
                     shift)
    
    param_lower_bounds <- c(rep(0, length(L_0_start)),
                            rep(10, 2),
                            rep(min_allowed_kd, 2), 
                            rep(-100, num_conc))
    
    param_upper_bounds <- c(rep(max_RU_tol, length(L_0_start)),
                            rep(10^7,2), 
                            rep(1, 2),
                            rep(100, num_conc))
    
  } else { if(!bulkshift & regenerated_surface){
    init_params <- c(L_0_start,
                      ka_g2_start, ka_b2_start, 
                      kd_g2_start, kd_b2_start)
    
    param_lower_bounds <- c(rep(0, length(L_0_start)),
                            rep(10, 2),
                            rep(min_allowed_kd, 2))
    
    param_upper_bounds <- c(rep(max_RU_tol, length(L_0_start)),
                            rep(10^7,2), 
                            rep(1, 2))
  }}}}
  
  names(init_params) <- rep("", length(init_params))
  fit_result <- minpack.lm::nls.lm(init_params,
                                   fn = fit_as_system_para, 
                                   pars_known = pars_known,
                                   df = df,
                                   incl_concentrations = incl_concentrations,
                                   num_conc = num_conc,
                                   association = association,
                                   bulkshift,
                                   global_rmax = global_rmax,
                                   # regenerated_surface = regenerated_surface,
                                   control = minpack.lm::nls.lm.control(maxfev = 10000,
                                                                        maxiter = max_iterations,
                                                                        ptol = ptol,
                                                                        ftol = ftol),
                                   lower = param_lower_bounds,
                                   jac = NULL,
                                   upper = param_upper_bounds)
  
  pars <- unlist(fit_result$par)
  
  if(global_rmax){
    
    L_0 <-  pars[1]
    
    parameter_values <- c(pars_known,
                          ka_g2 =  pars[2], ka_b2 =  pars[3],
                          kd_g2 =  pars[4], kd_b2 =  pars[5])
    
    if (bulkshift) {
      shift <- pars[6:(5 + num_conc)]
    } else {
      shift <- bulkshift
    }
    
  } else{
    L_0 <-  pars[1:num_conc]
    
    parameter_values <- c(pars_known,
                          ka_g2 =  pars[num_conc + 1],
                          ka_b2 =  pars[num_conc + 2],
                          kd_g2 =  pars[num_conc + 3],
                          kd_b2 =  pars[num_conc + 4])
    
    if (bulkshift) {
      shift <- pars[(num_conc + 5):(2*num_conc + 4)]
    } else {
      shift <- bulkshift
    }
  }
  
  fit_outcomes <- get_fit_outcomes_para(L_0, parameter_values, df, num_conc,
                                   incl_concentrations,
                                   association = association,
                                   shift = shift,
                                   global_rmax = global_rmax)
  
  list("FitResult" = fit_result, 
       "FitOutcomes" = fit_outcomes,
       "Initial" = init_params)
}


combine_output_para <- function(well_idx, fits_list, plot_list_out, rc_list, sample_info){
  
  if (!is.null(fits_list[[well_idx]]$error))
    return(NULL)
  
  if (sample_info[well_idx,]$`Global Rmax` == "Y")
    global_rmax <- TRUE else
      global_rmax <- FALSE
    
    if (sample_info[well_idx,]$`Bulkshift` == "Y")
      bulkshift <- TRUE else
        bulkshift <- FALSE
      
      if (sample_info[well_idx,]$Regen. == "Y" | sample_info[well_idx, ]$BaselineNegative)
        regenerated_surface <- TRUE else
          regenerated_surface <- FALSE
        
        num_conc <- sample_info[well_idx,]$NumInclConc
        
        if (global_rmax)
          Rmax_label <- "Rmax" else
            Rmax_label <- purrr::map_dfr(tibble::tibble(1:num_conc), function(x) paste("Rmax", x))
        
        if (!regenerated_surface)
          R0_label <- purrr::map_dfr(tibble::tibble(1:num_conc), function(x) paste("R_0", x)) else
            R0_label <- NULL
        bulkshift_label <- purrr::map_dfr(tibble::tibble(1:num_conc), function(x) paste("Bulkshift", x))
        
        
        pars <- unlist(fits_list[[well_idx]]$result$FitResult$par)
        
        if (bulkshift)
          par_names <- purrr::as_vector(purrr::flatten(c(Rmax_label, "ka", R0_label, "kd", bulkshift_label))) else
            par_names <- purrr::as_vector(purrr::flatten(c(Rmax_label, "ka", R0_label, "kd")))
        
        # result_summary <- summary(fits_list[[well_idx]]$result$FitResult)
        #R's built-in summary method doesn't play nicely when the some of the parameters hit their limiting values (the hessian is singular)
        # I've adapted the function to return NA's for std error when the limits are reached.
        
        result_summary <- summary_fit_with_constraints(fits_list[[well_idx]]$result$FitResult)
        #summary_fit_with_constraints returns the coefficients table from summary.minpack.lm
        
        summary_names <- colnames(result_summary)
        result_summary %>% tibble::as_tibble() -> par_err_table
        
        colnames(par_err_table) <- summary_names
        par_err_table <- suppressMessages(dplyr::bind_cols(Names = par_names, par_err_table))
        
        par_err_table %>%
          dplyr::filter(!stringr::str_detect(.data$Names,"R_0")) -> par_err_table
        par_err_table %>%
          dplyr::filter(!stringr::str_detect(.data$Names,"Bulkshift")) -> par_err_table
        
        
        par_names <- par_err_table$Names
        
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_g1") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> ka_g1
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_g1") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> ka_g1_se       
        
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_b1") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> ka_b1
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_b1") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> ka_b1_se
        
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_g2") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> ka_g2
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_g2") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> ka_g2_se   
        
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_b2") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> ka_b2
        par_err_table %>%
          dplyr::filter(.data$Names == "ka_b2") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> ka_b2_se
        
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_g1") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> kd_g1
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_g1") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> kd_g1_se
        
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_b1") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> kd_b1
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_b1") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> kd_b1_se   
        
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_g2") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> kd_g2
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_g2") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> kd_g2_se    
        
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_b2") %>%
          dplyr::select("Estimate") %>%
          as.numeric() -> kd_b2
        par_err_table %>%
          dplyr::filter(.data$Names == "kd_b2") %>%
          dplyr::select("Std. Error") %>%
          as.numeric() -> kd_b2_se
        
        # write a for loop?
        KD_g1 <- kd_g1/ka_g1
        KD_g1_se <- KD_g1 * ((ka_g1_se/ka_g1)^2 + (kd_g1_se/kd_g1)^2)^(1/2)  
        
        KD_b1 <- kd_b1/ka_b1
        KD_b1_se <- KD_b1 * ((ka_se_b1/ka)^2 + (kd_se_b1/kd_b1)^2)^(1/2) 
        
        KD_g2 <- kd_g2/ka_g2
        KD_g2_se <- KD_g2 * ((ka_se_g2/ka_g2)^2 + (kd_se_g2/kd)^2)^(1/2)      
        
        KD_b2 <- kd_b2/ka_b2
        KD_b2_se <- KD_b2 * ((ka_se_b2/ka_b2)^2 + (kd_se_b2/kd_b2)^2)^(1/2)
        
        if (kd_g1 == 10^(-5)){
          kd_g1_se <- NA
          KD_g1_se <- NA
          idx <- which(par_err_table$Names == "kd_g1")
          par_err_table$`Std. Error`[idx] <- NA
        }        
        
        if (kd_b1 == 10^(-5)){
          kd_b1_se <- NA
          KD_b1_se <- NA
          idx <- which(par_err_table$Names == "kd_b1")
          par_err_table$`Std. Error`[idx] <- NA
        }      
        
        if (kd_g1 == 10^(-5)){
          kd_g1_se <- NA
          KD_g1_se <- NA
          idx <- which(par_err_table$Names == "kd_g1")
          par_err_table$`Std. Error`[idx] <- NA
        }        
        
        if (kd_b2 == 10^(-5)){
          kd_b2_se <- NA
          KD_b2_se <- NA
          idx <- which(par_err_table$Names == "kd_b2")
          par_err_table$`Std. Error`[idx] <- NA
        }
        
        par_err_table %>%
          dplyr::filter(grepl("ka|kd", Names)) %>%
          dplyr::select("Estimate", "Std. Error")  %>%
          dplyr::mutate(Estimate = format(signif(.data$Estimate, 3),big.mark=",",decimal.mark=".", scientific = TRUE)) %>%
          dplyr::mutate(`Std. Error` = format(signif(.data$`Std. Error`, 3),big.mark=",",decimal.mark=".", scientific = TRUE)) -> kakd_out
        
        par_err_table %>%
          dplyr::filter(grepl("ka|kd", Names)) %>%
          dplyr::select("Estimate", "Std. Error")  %>%
          dplyr::mutate(Estimate = format(round(.data$Estimate,2),big.mark=",",decimal.mark=".", scientific = FALSE))  %>%
          dplyr::mutate(`Std. Error` = format(round(.data$`Std. Error`, 2),big.mark=",",decimal.mark=".", scientific = FALSE)) -> rest_out
        
        
        KD_tbl <- tibble::tibble(Estimate = KD, `Std. Error` = KD_se)
        
        KD_tbl %>%
          dplyr::select("Estimate", "Std. Error")  %>%
          dplyr::mutate(Estimate = format(signif(.data$Estimate, 3),big.mark=",",decimal.mark=".", scientific = TRUE)) %>%
          dplyr::mutate(`Std. Error` = format(signif(.data$`Std. Error`, 3),big.mark=",",decimal.mark=".", scientific = TRUE)) -> KD_tbl
        
        par_names <- c(par_names, "KD")
        dplyr::bind_rows(rest_out, kakd_out) %>%
          dplyr::bind_rows(KD_tbl) %>%
          gridExtra::tableGrob(rows = par_names,
                               theme = gridExtra::ttheme_minimal(core=list(fg_params=list(hjust=1, x=0.9)))) -> tb1
        
        RU_resid <- fits_list[[well_idx]]$result$FitResult$fvec
        fits_list[[well_idx]]$result$FitOutcomes$Time -> Time_resid
        fits_list[[well_idx]]$result$FitOutcomes$Concentration -> Concentration_resid
        
        
        resid_plot <- ggplot2::ggplot(data = tibble::tibble(Residuals = RU_resid,
                                                            Time = Time_resid,
                                                            Concentration = forcats::as_factor(Concentration_resid)),
                                      ggplot2::aes(x = .data$Time, y = .data$Residuals, color = .data$Concentration)) + ggplot2::geom_point(size = 0.01) +
          ggplot2::ggtitle(label = "Residuals")
        
        gridExtra::grid.arrange(plot_list_out[[well_idx]], tb1, resid_plot,
                                rc_list[[well_idx]], ncol=2)
}

print_output_para <- function(well_idx, pages_list, plot_list_out, sample_info){
  
  if (is.null(pages_list[[well_idx]]$error) & !is.null(pages_list[[well_idx]]$result))
    return(gridExtra::arrangeGrob(pages_list[[well_idx]]$result))
  
  err_msg <- paste("The following well has an unrecoverable error:",
                   well_idx,
                   "Block ", sample_info[well_idx,]$Block,
                   "Row", sample_info[well_idx,]$Row,
                   "ROI", sample_info[well_idx,]$ROI)
  err_msg <- paste0(err_msg, sample_info$Column)
  
  if (is.null(plot_list_out[[well_idx]]))
    return(p1 = gridExtra::arrangeGrob(grid::textGrob(err_msg)))
  else
    return(p1 = gridExtra::arrangeGrob(grid::textGrob(err_msg), plot_list_out[[well_idx]]))
  
}


# test: fits_list = PGT121_VRC07_fits
# sample_info <- sample_info_fits
# well_idx <- 1
get_csv_para <- function(well_idx, fits_list, sample_info){
  
  num_conc <- sample_info[well_idx,]$NumInclConc
  
  if (sample_info[well_idx,]$`Global Rmax` == "Y")
    global_rmax <- TRUE else
      global_rmax <- FALSE
    
    
    if (sample_info[well_idx, ]$Bulkshift == "Y")
      bulkshift <- TRUE else
        bulkshift <- FALSE
      
      if (sample_info[well_idx, ]$`Regen.` == "Y" | sample_info[well_idx,]$BaselineNegative)
        regenerated_surface <- TRUE else
          regenerated_surface <- FALSE
        
        
        if (!global_rmax){
          L_0 <- rep(NA, 5)
          L_0_se <- rep(NA,5)
        } else {
          L_0 <- NA
          L_0_se <- NA
        }
        
        Bulkshift <- rep(NA, 5)
        Bulkshift_se <- rep(NA,5)
        
        # Problem: needed?
        # R0 <- rep(NA,5)
        # R0_se <- rep(NA,5)
        
        # check for 1
        if (is.null(fits_list[[well_idx]]$result$FitResult)){
          return(c(L_0 = L_0, L_0_se = L_0_se, 
                   ka_g1 = NA, ka_g1_se = NA, ka_b1 = NA, ka_b1_se = NA,
                   kd_g1 = NA, kd_g1_se = NA, kd_b1 = NA, kd_b1_se = NA,
                   ka_g2 = NA, ka_g2_se = NA, ka_b2 = NA, ka_b2_se = NA,
                   ka_g2 = NA, ka_g2_se = NA, ka_b2 = NA, ka_b2_se = NA,
                   Bulkshift = Bulkshift, Bulkshift_se = Bulkshift_se, 
                   R0 = R0, R0_se = R0_se))
          
        }
        
        pars <- unlist(fits_list[[well_idx]]$result$FitResult$par)

        result_summary <- summary_fit_with_constraints(fits_list[[well_idx]]$result$FitResult)
        
        # first column of summary is the estimate. Second is standard error
        
        summary_names <- colnames(result_summary)
        result_summary %>% tibble::as_tibble() %>% dplyr::select("Estimate", "Std. Error") -> par_err_table
        
        colnames(par_err_table) <- summary_names[1:2]
        
        if (global_rmax){
          L_0 <- par_err_table[1,]$Estimate
          L_0_se <- par_err_table[1,]$`Std. Error`
          curr_idx <- 2
          
        } else {
          L_0[1:num_conc] <- par_err_table[1:num_conc,]$Estimate
          L_0_se[1:num_conc] <- par_err_table[1:num_conc,]$`Std. Error`
          curr_idx <- num_conc + 1
          
        }
        
        ka_g2 <- par_err_table[curr_idx,]$Estimate
        ka_g2_se <- par_err_table[curr_idx,]$`Std. Error`
        curr_idx <- curr_idx + 1  
        
        ka_b2 <- par_err_table[curr_idx,]$Estimate
        ka_b2_se <- par_err_table[curr_idx,]$`Std. Error`
        curr_idx <- curr_idx + 1    
        
        # deleted R0
        
        kd_g2 <- par_err_table[curr_idx,]$Estimate
        kd_g2_se <- par_err_table[curr_idx,]$`Std. Error`
        curr_idx <- curr_idx + 1  
        
        kd_b2 <- par_err_table[curr_idx,]$Estimate
        kd_b2_se <- par_err_table[curr_idx,]$`Std. Error`
        curr_idx <- curr_idx + 1
        
        if (bulkshift){
          Bulkshift[1:num_conc] <- par_err_table[curr_idx:(curr_idx + num_conc - 1), ]$Estimate
          Bulkshift_se[1:num_conc] <- par_err_table[curr_idx:(curr_idx + num_conc - 1), ]$`Std. Error`
        }
        
        KD_g2 <- kd_g2/ka_g2
        KD_g2_se <- KD_g2 * ((ka_g2_se/ka_g2)^2 + (kd_g2_se/kd_g2)^2)^(1/2)
        
        if (kd_g2 == 10^(-5)){
          kd_g2_se <- NA
          KD_g2_se <- NA
        }   
        
        KD_b2 <- kd_b2/ka_b2
        KD_b2_se <- KD_b2 * ((ka_b2_se/ka_b2)^2 + (kd_b2_se/kd_b2)^2)^(1/2)
        
        if (kd_b2 == 10^(-5)){
          kd_b2_se <- NA
          KD_b2_se <- NA
        }
        
        
        c(ROI = sample_info[well_idx,]$ROI,
          L_0 = suppressWarnings(as.numeric(round(L_0, 2))),
          L_0_se = suppressWarnings(as.numeric(round(L_0_se, 2))),
          ka_g2 = suppressWarnings(as.numeric(round(ka_g2,2))),
          ka_g2_se = suppressWarnings(as.numeric(round(ka_g2_se, 2))),
          kd_g2 = suppressWarnings(as.numeric(format(signif(kd_g2, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          kd_g2_se = suppressWarnings(as.numeric(format(signif(kd_g2_se, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          KD_g2 = suppressWarnings(as.numeric(format(signif(KD_g2, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          KD_g2_se = suppressWarnings(as.numeric(format(signif(KD_g2_se, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          ka_b2 = suppressWarnings(as.numeric(round(ka_b2,2))),
          ka_b2_se = suppressWarnings(as.numeric(round(ka_b2_se, 2))),
          kd_b2 = suppressWarnings(as.numeric(format(signif(kd_b2, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          kd_b2_se = suppressWarnings(as.numeric(format(signif(kd_b2_se, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          KD_b2 = suppressWarnings(as.numeric(format(signif(KD_b2, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          KD_b2_se = suppressWarnings(as.numeric(format(signif(KD_b2_se, 3),big.mark=",",decimal.mark=".", scientific = TRUE))),
          Bulkshift = suppressWarnings(as.numeric(round(Bulkshift, 2))),
          Bulkshift_se = suppressWarnings(as.numeric(round(Bulkshift_se, 2))))
        # deleted: R0
}
